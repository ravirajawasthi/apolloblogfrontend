import React, { createContext, useReducer } from "react";
import jwt from "jsonwebtoken";
import configObj from "../config";

const initialState = { user: null };
const authToken = localStorage.getItem("jwtToken");

if (authToken) {
  jwt.verify(authToken, configObj.secretKey, (_err, verifiedToken) => {
    if (_err) localStorage.removeItem('jwtToken');
    else initialState.user = verifiedToken;
  });
}

const AuthContext = createContext({
  user: initialState.user,
  login: (data) => {},
  logout: () => {},
});

function authReducer(state, action) {
  switch (action.type) {
    case "LOGIN": {
      console.log(action.payload)
      return {
        ...state,
        user: action.payload,
      };
    }
    case "LOGOUT": {
      return {
        ...state,
        user: null,
      };
    }
    default:
      return state;
  }
}

function AuthProvider(props) {
  const [state, dispatch] = useReducer(authReducer, initialState);
  const login = (userData) => {
    localStorage.setItem("jwtToken", userData.token);
    dispatch({ type: "LOGIN", payload: userData });
  };
  const logout = () => {
    dispatch({ type: "LOGOUT" });
    localStorage.removeItem("jwtToken");
  };

  return (
    <AuthContext.Provider value={{ user: state.user, login, logout }}>
      {props.children}
    </AuthContext.Provider>
  );
}
export { AuthContext, AuthProvider };
