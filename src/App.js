import React, { useContext } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import "semantic-ui-css/semantic.min.css";
import "./App.css";

import { Container } from "semantic-ui-react";
import MenuBar from "./components/MenuBar";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import ApolloClient from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { createHttpLink } from "apollo-link-http";
import { setContext } from "apollo-link-context";
import { AuthContext } from "./context/auth.context";
import { ApolloProvider } from "@apollo/react-hooks";

const httpLink = createHttpLink({
  uri: "http://localhost:5000",
});

function App() {
  const authContext = useContext(AuthContext);

  const authLink = setContext(() => {
    const httpParams = { headers: {} };
    const jwtToken = localStorage.getItem("jwtToken");
    if (jwtToken) {
      httpParams.headers["Authorization"] = `Bearer ${jwtToken}`;
    }
    return httpParams;
  });
  const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache(),
  });
  return (
    <ApolloProvider client={client}>
      <Router>
        <Container>
          <MenuBar />
          <Route
            exact
            path="/"
            render={(routeProps) => <Home {...routeProps} />}
          />
          <Route
            exact
            path="/login"
            render={(routeProps) => <Login {...routeProps} />}
          />
          <Route
            exact
            path="/register"
            render={(routeProps) => <Register {...routeProps} />}
          />
        </Container>
      </Router>
    </ApolloProvider>
  );
}

export default App;
