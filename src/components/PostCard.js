import React, { useState, useContext } from "react";

import DeleteDialog from "./DeleteDialog";

import moment from "moment";
import { Card, Icon, Label, Image, Button } from "semantic-ui-react";
import { Link } from "react-router-dom";

import { useMutation } from "@apollo/react-hooks";
import { LIKE_POST } from "../util/GraphQlQueries";
import { withRouter } from "react-router-dom";
import { AuthContext } from "../context/auth.context";

function PostCard(props) {
  const {
    body,
    createdAt,
    id,
    username,
    likeCount,
    commentCount,
    likes,
  } = props.post;
  const authContext = useContext(AuthContext);
  const [liked, setLiked] = useState(
    authContext.user &&
      likes.filter((like) => like.username === authContext.user.username)
        .length > 0
  );
  const [likePost, { error }] = useMutation(LIKE_POST, {
    onError() {
      props.history.push("/login");
    },
    onCompleted(_data) {
      if (!error) setLiked(!liked);
    },
    variables: { postId: id },
  });

  const handleLike = () => {
    likePost();
  };

  const handleComment = () => {
    props.history.push(`/post/${id}`);
  };

  const deleteDialogMarkup = authContext.user &&
    authContext.user.username === username && (
      <DeleteDialog size="mini" postId={id} />
    );

  return (
    <Card fluid>
      <Card.Content>
        <Image
          floated="right"
          size="mini"
          src="https://react.semantic-ui.com/images/avatar/large/molly.png"
        />
        <Card.Header>{username}</Card.Header>
        <Card.Meta as={Link} to={`/posts/${id}`}>
          {moment(createdAt).fromNow()}
        </Card.Meta>
        <Card.Description>{body}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Button as="div" labelPosition="right" onClick={handleLike}>
          <Button color="teal" basic={!liked}>
            <Icon name="heart" />
          </Button>
          <Label as="a" color="teal" basic pointing="left">
            {likeCount}
          </Label>
        </Button>

        <Button as="div" labelPosition="right" onClick={handleComment}>
          <Button color="blue" basic>
            <Icon name="comments" />
          </Button>
          <Label as="a" basic color="blue" pointing="left">
            {commentCount}
          </Label>
        </Button>
        {deleteDialogMarkup}
      </Card.Content>
    </Card>
  );
}

export default withRouter(PostCard);
