import React, { useState } from "react";
import "./styles/NewPostFormStyles.css";
import { NEW_POST, FETCH_POSTS_QUERY } from "../util/GraphQlQueries";

import { useMutation } from "@apollo/react-hooks";
import { Form, Button, Card } from "semantic-ui-react";

function NewPost() {
  const [newPostBody, setNewPostBody] = useState("");
  const [errors, setErrors] = useState({});
  const [createPost, { loading }] = useMutation(NEW_POST, {
    update(proxy, { data: { createPost: newPost } }) {
      const data = proxy.readQuery({ query: FETCH_POSTS_QUERY });
      proxy.writeQuery({
        query: FETCH_POSTS_QUERY,
        data: {
          getPosts: [newPost, ...data.getPosts],
        },
      });
      setNewPostBody("");
      setErrors({});
    },
    onError(err) {
      console.log(err);
      setErrors(err.graphQLErrors[0].extensions.errors);
    },
    variables: { body: newPostBody },
    refetchQueries: (refetchPosts) => [{ query: FETCH_POSTS_QUERY }],
  });

  const handleInputChange = (e) => {
    setNewPostBody(e.target.value);
  };

  const handlePostSubmit = (e) => {
    e.preventDefault();
    createPost();
  };

  return (
    <Card className="NewPostForm-root">
      <Form
        className="NewPostForm-form"
        loading={loading}
        onSubmit={handlePostSubmit}
      >
        <Form.Input
          label="New Post"
          required
          value={newPostBody}
          error={errors.body}
          onChange={handleInputChange}
        />
        <Button type="submit" primary>
          Post!
        </Button>
      </Form>
    </Card>
  );
}

export default NewPost;
