import React, { useContext } from "react";
import { Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { AuthContext } from "../context/auth.context";
import { withRouter } from "react-router-dom";
function MenuBar(props) {
  const authContext = useContext(AuthContext);
  let path = props.location.pathname
  
  const menubar = authContext.user ? (
    <Menu pointing secondary size="massive" color="teal">
      <Menu.Item name={authContext.user.username} active as={Link} to="/" />
      <Menu.Menu position="right">
        <Menu.Item
          position="right"
          name="Logout"
          onClick={authContext.logout}
          as={Link}
          to="/"
        />
      </Menu.Menu>
    </Menu>
  ) : (
    <Menu pointing secondary size="massive" color="teal">
      <Menu.Item name="home" active={path === "/"} as={Link} to="/" />
      <Menu.Menu position="right">
        <Menu.Item
          position="right"
          name="login"
          active={path === "/login"}
          as={Link}
          to="/login"
        />
        <Menu.Item
          position="right"
          name="register"
          active={path === "/register"}
          as={Link}
          to="/register"
        />
      </Menu.Menu>
    </Menu>
  );

  return menubar;
}

export default withRouter(MenuBar);
