import React, { useState } from "react";
import "./styles/DeleteDialogStyles.css";
import { Modal } from "semantic-ui-react";
import { Icon } from "semantic-ui-react";
import { Button } from "semantic-ui-react";
import { DELETE_POST, FETCH_POSTS_QUERY } from "../util/GraphQlQueries";
import { useMutation } from "@apollo/react-hooks";

function DeleteDialog({ postId }) {
  console.log(postId);
  const [dialogOpen, setDialogOpen] = useState(false);
  const toggleDialog = () => setDialogOpen(!dialogOpen);
  const [deletePost] = useMutation(DELETE_POST, {
    update(proxy, _result) {
      let data = proxy.readQuery({ query: FETCH_POSTS_QUERY });
      proxy.writeQuery({
        query: FETCH_POSTS_QUERY,
        data: {
          getPosts: data.getPosts.filter((post) => post.id !== postId),
        },
      });
    },
    onError(err) {
      console.log(err);
    },
    onCompleted() {
      toggleDialog();
    },
    variables: {
      postId,
    },
  });

  const handlePostDelete = () => deletePost();

  return (
    <>
      <Icon
        bordered
        color="red"
        inverted
        name="trash alternate"
        onClick={toggleDialog}
        className="DeleteDialog-icon"
      />
      <Modal open={dialogOpen} onClose={toggleDialog} size="mini">
        <Modal.Header>Delete Post?</Modal.Header>
        <Modal.Content>
          <p>Are you sure you want to delete your post?</p>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={toggleDialog} negative>
            No
          </Button>
          <Button
            onClick={handlePostDelete}
            positive
            labelPosition="right"
            icon="checkmark"
            content="Yes"
          />
        </Modal.Actions>
      </Modal>
    </>
  );
}

export default DeleteDialog;
