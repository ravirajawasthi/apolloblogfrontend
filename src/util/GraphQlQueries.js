import gql from "graphql-tag";

export const NEW_POST = gql`
  mutation createPost($body: String!) {
    createPost(body: $body) {
      id
      body
      createdAt
      username

      likes {
        username
      }
      commentCount
      likeCount
      comments {
        id
        username
        createdAt
        body
      }
    }
  }
`;

export const FETCH_POSTS_QUERY = gql`
  {
    getPosts {
      id
      body
      createdAt
      username

      likes {
        username
      }
      commentCount
      likeCount
      comments {
        id
        username
        createdAt
        body
      }
    }
  }
`;

export const LOGIN_USER = gql`
  mutation login($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      id
      username
      email
      token
      createdAt
    }
  }
`;

export const REGISTER_USER = gql`
  mutation register(
    $username: String!
    $email: String!
    $password: String!
    $confirmPassword: String!
  ) {
    register(
      registerInput: {
        username: $username
        email: $email
        password: $password
        confirmPassword: $confirmPassword
      }
    ) {
      id
      username
      email
      token
      createdAt
    }
  }
`;

export const LIKE_POST = gql`
  mutation toggleLikeOnPost($postId: ID!) {
    toggleLikeOnPost(postId: $postId) {
      id
      body
      createdAt
      username
      likes {
        username
      }
      commentCount
      likeCount
    }
   }
`;

export const DELETE_POST = gql`
  mutation deletePost($postId: ID!) {
    deletePost(postId: $postId) 
   }
`;

