import React, { useState, useContext } from "react";
import "./styles/RegisterStyles.css";

import { Form, Button, Message } from "semantic-ui-react";

import {REGISTER_USER} from '../util/GraphQlQueries'
import { useMutation } from "@apollo/react-hooks";

import { AuthContext } from "../context/auth.context";

function Register(props) {
  const authContext = useContext(AuthContext);
  if (authContext.user) props.history.push("/");
  //==========================================================================================================================================
  const [errors, setErrors] = useState({});

  const [formValues, setFormValues] = useState({
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
  });

  //==========================================================================================================================================
  const handleInputChange = (e) => {
    setFormValues({ ...formValues, [e.target.name]: e.target.value });
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    addUser();
  };

  //==========================================================================================================================================

  const [addUser, { loading = {}, called, error }] = useMutation(
    REGISTER_USER,
    {
      update(proxy, { data: { register: registerInfo } }) {
        authContext.login(registerInfo);
        setErrors({});
        props.history.push("/");
      },
      onError(error) {
        setErrors(error.graphQLErrors[0].extensions.errors);
      },
      variables: formValues,
    }
  );

  //==========================================================================================================================================
  return (
    <Form
      className="Form-main"
      onSubmit={handleFormSubmit}
      noValidate
      loading={loading}
      error={Boolean(error)}
      success={Object.keys(errors).length === 0 && called}
    >
      <h1>Register</h1>
      <Form.Input
        label="Username"
        placeholder="Username..."
        name="username"
        value={formValues.username}
        onChange={handleInputChange}
        error={errors["username"]}
      />
      <Form.Input
        label="Email"
        placeholder="Email..."
        name="email"
        value={formValues.email}
        onChange={handleInputChange}
        error={errors["email"]}
      />
      <Form.Input
        type="password"
        label="Password"
        placeholder="Password..."
        name="password"
        value={formValues.password}
        onChange={handleInputChange}
        error={errors["password"]}
      />
      <Form.Input
        type="password"
        label="Confirm Password"
        placeholder="Confirm Password..."
        name="confirmPassword"
        value={formValues.confirmPassword}
        onChange={handleInputChange}
        error={errors["confirmPassword"]}
      />
      <Button primary type="submit">
        Primary
      </Button>

      <Message
        key="success"
        success
        header="Howdy!💯😎"
        content="Thanks for signing up for our revolutionary platform! you will be redirected in 3 seconds...🎉🎁"
      />

      <Message
        key="error"
        error
        header="Invalid Form Values"
        list={Object.keys(errors).map((error) => errors[error])}
      />
    </Form>
  );
}

export default Register;

