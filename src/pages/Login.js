import React, { useState, useContext } from "react";
import { Form, Button, Message } from "semantic-ui-react";
import {LOGIN_USER} from '../util/GraphQlQueries'

import { useMutation } from "@apollo/react-hooks";

import { AuthContext } from "../context/auth.context";

function Login(props) {
  const authContext = useContext(AuthContext);
  if (authContext.user) props.history.push("/");

  const [loginCredentials, setLoginCredentials] = useState({
    username: "",
    password: "",
  });

  const [errors, setErrors] = useState({});

  const [loginUser, { called, loading }] = useMutation(LOGIN_USER, {
    onError(error) {
      setErrors(error.graphQLErrors[0].extensions.errors);
    },
    update(_proxy, { data: { login: loginData } }) {
      setErrors({});
      authContext.login(loginData);
      props.history.push("/");
    },
    variables: loginCredentials,
  });

  const handleInputChange = (e) => {
    setLoginCredentials({
      ...loginCredentials,
      [e.target.name]: e.target.value,
    });
  };

  const handleLogin = (e) => {
    e.preventDefault();
    loginUser();
  };

  return (
    <Form
      onSubmit={handleLogin}
      className="Form-main"
      loading={loading}
      error={Object.keys(errors).length > 0}
    >
      <h1>Login</h1>
      <Form.Input
        label="Username"
        name="username"
        type="text"
        onChange={handleInputChange}
        value={loginCredentials.username}
        error={errors.username || Boolean(errors.general)}
      />
      <Form.Input
        label="Password"
        name="password"
        type="password"
        onChange={handleInputChange}
        value={loginCredentials.password}
        error={errors.password || Boolean(errors.general)}
      />
      <Button primary type="submit">
        Login!
      </Button>
      <Message
        error
        header="Invalid Credentials"
        list={Object.keys(errors).map((error) => errors[error])}
      />
    </Form>
  );
}

export default Login;

