import React, { useContext } from "react";
import { AuthContext } from "../context/auth.context";
import "./styles/HomeStyles.css";

import { useQuery } from "@apollo/react-hooks";
import { FETCH_POSTS_QUERY } from "../util/GraphQlQueries";

import { Grid, Transition } from "semantic-ui-react";
import PostCard from "../components/PostCard";
import NewPostForm from "../components/NewPostForm";

function Home(props) {
  const authContext = useContext(AuthContext);
  const { loading, data: { getPosts: posts } = {} } = useQuery(
    FETCH_POSTS_QUERY
  );
  const newPostFormMarkup = authContext.user ? (
    <Grid.Column className="Home-Column" key="newPostForm">
      <NewPostForm />
    </Grid.Column>
  ) : null;

  const postListMarkup =
    !loading &&
    posts.map((post) => (
      <Grid.Column className="Home-Column" key={post.id}>
        <PostCard post={post} />
      </Grid.Column>
    ));

  return (
    <Grid columns={3}>
      <Grid.Row className="Home-title">
        <h1>Recent Posts</h1>
      </Grid.Row>
      <Grid.Row>
        {loading ? (
          <h1>Loading posts...</h1>
        ) : (
          <>
            {newPostFormMarkup}
            {postListMarkup}
          </>
        )}
      </Grid.Row>
    </Grid>
  );
}

export default Home;
